#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

struct Node *createNode(int number)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = number;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head)
{
    struct Node *current = head;
    printf("[");
    while (current != NULL)
    {
        printf("%d", current->number);
        current = current->next;

        if (current != NULL)
        {
            printf(", ");
        }
    }
    printf("]\n");
}

void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}

void deleteByKey(struct Node **head, int key)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    // If the key is in the head node
    if (current != NULL && current->number == key)
    {
        *head = current->next;
        free(current);
        return;
    }

    // Search for the key to be deleted
    while (current != NULL && current->number != key)
    {
        prev = current;
        current = current->next;
    }

    // If key was not present in the list
    if (current == NULL)
    {
        printf("Key not found in the list\n");
        return;
    }

    // Unlink the node from the linked list
    prev->next = current->next;

    // Free the memory
    free(current);
}

void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;
    while (current != NULL && current->number != key)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Key not found in the list\n");
        return;
    }

    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;
    while (current != NULL && current->number != searchValue)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Value not found in the list\n");
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}

int main()
{
    struct Node *head = NULL;
    int choice, data, key, newValue, searchValue;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Insert After Key\n");
        printf("6. Insert After Value\n");
        printf("7. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;

        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;

        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;

        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
            break;

        case 5:
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &newValue);
            insertAfterKey(&head, key, newValue);
            break;

        case 6:
            printf("Enter value to search: ");
            scanf("%d", &searchValue);
            printf("Enter value to insert: ");
            scanf("%d", &newValue);
            insertAfterValue(&head, searchValue, newValue);
            break;

        case 7:
            printf("Exiting program.\n");
            exit(0);

        default:
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
